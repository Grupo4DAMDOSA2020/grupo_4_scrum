package main;

import java.awt.event.*;

import java.util.Random;

//import javax.swing.JPanel;
import javax.swing.JOptionPane;


import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MainYAMP {

	public static void main(String[] args) {
		
		//Declaracion del JLabel para la imagen
		JLabel background;
		JLabel personaje;
		JLabel mosca;
		//Declaracion de la imagen de fondo en cuestion
		ImageIcon img = new ImageIcon("images/mapa.jpg");
		ImageIcon pj = new ImageIcon("images/cebollo.png");
		ImageIcon imosca = new ImageIcon("images/mosca.png");
		//Variables para la posicion del personaje
		int per_pos_Y = 150;
		int per_pos_X = 250;
		
		//Definicion de background para funcionar como un JLabel
		background = new JLabel("",img,JLabel.CENTER);
		personaje = new JLabel("",pj,JLabel.CENTER);
		mosca = new JLabel("",imosca,JLabel.CENTER);
		//Atributos de background funcionando como JLabel (pos X, pos Y, Alto, Ancho
		background.setBounds(0, 0, 600, 400);
		personaje.setBounds(per_pos_X, per_pos_Y, 50, 50);
		mosca.setBounds(0, 0, 40, 40);
		
		//Declaracion de la ventana
		JFrame ventana = new JFrame("YAMP"); //Titulo YAMP
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cerramos el proceso al pulsar X
		ventana.setVisible(true); 	//hacer visible la ventana
		ventana.setResizable(false);
		ventana.setSize(600, 400); //Establecemos una medida
		
		//Adicion de JLabel Background a la ventana
		ventana.add(mosca);
		ventana.add(personaje);
		ventana.add(background); 
		
		/*
		while(true) {
			movermosca();
			ventana.repaint();
		}
		*/
		
		LeerTeclado tecla = new LeerTeclado(personaje, mosca);
		//ventana.add(new LeerTeclado());

		
		ventana.addKeyListener(tecla); 
		//personaje.getlocation();
		
		
	}
	
}

	


class LeerTeclado extends MainYAMP implements KeyListener{
	JLabel personaje;
	JLabel mosca;
	static int contador = 0;
	static int limite = 20;

	public LeerTeclado(JLabel personaje, JLabel mosca) {
		this.mosca = mosca;
		this.personaje = personaje;
		personaje.setFocusable(true);
		spawn_mosca(mosca);
		
		
	}
	
	
	//M�todos de key listener 
	//Pulsar y soltar
	public void keyTyped(KeyEvent e) {
		
	}
	
	
	//Al pulsar
	public void keyPressed(KeyEvent e) {
		
		contador ++;
		
		if (contador >= limite) {
			spawn_mosca(mosca);
			contador = 0;
		}
	
		char tecla_pulsada = e.getKeyChar();
		System.out.println("Tecla pulsada: " + tecla_pulsada);
		
		int posX = personaje.getX();
		int posY = personaje.getY();
		
		//MOVIMIENTOS RECTILINEOS
		if (tecla_pulsada == 'w' || tecla_pulsada == 'W') {
			if (posY > 5) {
			personaje.setBounds(posX, posY - 10, 50, 50);
			comprobar_posiciones(mosca, personaje);
			}
		}
		
		if (tecla_pulsada == 'a' ||  tecla_pulsada == 'A') {
			if (posX > 5) {
			personaje.setBounds(posX - 10, posY, 50, 50);
			comprobar_posiciones(mosca, personaje);
			}
		}		
				 
		if (tecla_pulsada == 's' || tecla_pulsada == 'S') {
			if (posY < 305) {
			personaje.setBounds(posX, posY + 10, 50, 50);
			comprobar_posiciones(mosca, personaje);
			}
		}
		
		if (tecla_pulsada == 'd' ||  tecla_pulsada == 'D') {
			if (posX < 525) {
			personaje.setBounds(posX + 10, posY, 50, 50);
			comprobar_posiciones(mosca, personaje);
			}
		}
			
		//MOVIMIENTOS DIAGONALES
		
		if ((tecla_pulsada == 'a' ||  tecla_pulsada == 'A') && (tecla_pulsada == 'w' || tecla_pulsada == 'W')) {
			if (posX > 5 && posY > 5) {
			personaje.setBounds(posX - 10, posY - 10, 50, 50);
			comprobar_posiciones(mosca, personaje);
			}
		}
		
		if ((tecla_pulsada == 'a' ||  tecla_pulsada == 'A') && (tecla_pulsada == 's' || tecla_pulsada == 'S')) {
			if (posX > 5 && posY > 5) {
			personaje.setBounds(posX - 10, posY + 10, 50, 50);
			comprobar_posiciones(mosca, personaje);
			}
		}
		
		if ((tecla_pulsada == 'd' ||  tecla_pulsada == 'D') && (tecla_pulsada == 'w' || tecla_pulsada == 'W')) {
			if (posX > 5 && posY > 5) {
			personaje.setBounds(posX + 10, posY - 10, 50, 50);
			comprobar_posiciones(mosca, personaje);
			}
		}
		
		if ((tecla_pulsada == 'd' ||  tecla_pulsada == 'D') && (tecla_pulsada == 's' || tecla_pulsada == 'S')) {
			if (posX > 5 && posY > 5) {
			personaje.setBounds(posX + 10, posY + 10, 50, 50);
			comprobar_posiciones(mosca, personaje);
			}
		}
	}
	
	
	//Al soltar
	public void keyReleased(KeyEvent e) {
		
	}

	
	private static void spawn_mosca(JLabel mosca) {
		//Movimiento aleatorio de la mosca
		Random aleatorio = new Random();
		int xMosca = aleatorio.nextInt(500);
		int yMosca = aleatorio.nextInt(300);
		mosca.setBounds(xMosca, yMosca, 40, 40);
		System.out.println("xMosca = " + xMosca + "\nyMosca = " + yMosca);
	}
	
	private static void comprobar_posiciones(JLabel mosca, JLabel personaje) {
		//Comprobar si lamosca y el pj est�n cerca
		int xMosca = mosca.getX();
		int yMosca = mosca.getY();
		int xPj = personaje.getX();
		int yPj = personaje.getY();
		
		//Razonamiento: eliminar una cifra de 3 -> 200 a 20 y al dividir entre diez y entre 2 para ser 5 y dar asi un rango entre el pj y la mosca
		if ((xMosca/20) == (xPj/20) && (yMosca/20) == (yPj/20) ) {
			System.out.println("HAS GANADO");
			JOptionPane.showMessageDialog(null,"FELICIDADES \n���HAS GANADO!!!");
			spawn_mosca(mosca);
			contador = 0;
			//Dificultad que baja progresivamente vayas ganando
			if (limite > 5) {
				limite --;
			}
			if (limite < 5) {
				limite = 5;
			}
			
		}
	}
}



