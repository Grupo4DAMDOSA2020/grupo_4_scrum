package com.iframe;
// >   < 
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
//Loop para el refresco del juego
//Referencia a Canvas para el tratamiento simple de graficos
public class Game extends Canvas implements Runnable{
	 
	private static final long serialVersionUID = 4772498530659229334L;

	public static final int WIDTH = 640, HEIGHT = WIDTH / 12 * 9;
	private Thread thread;
	private boolean running = false;
	//Llamada a Window.java
	public Game(){
		new Window(WIDTH, HEIGHT, "A ver si hacemos un Isaac...", this);
	}
	
	//Comprobacion de estado, reporte de bugs gracias a "e"
	public synchronized void start(){
		thread = new Thread(this);
		thread.start();
		running = true;
	}
	
	public synchronized void stop(){
		try {
			thread.join();
			running = false;
		}catch(Exception e){
			e.printStackTrace();
		}
	}
		
	//Elementos para el renderizador
	public void run() {
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		int frames = 0;
		//Contador de fotogramas
		while(running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while(delta >= 1) {
				tick();
				delta--;
			}
			if (running)
				render();
			frames++;
			
			if(System.currentTimeMillis() - timer > 1000){
				timer += 1000;
				System.out.println("FPS actuales: " + frames);
				frames = 0;
			}
		}
		stop();
	}
	
	//Metodo tick
	private void tick(){
		
	}
	
	//metodo render
	private void render(){
		
		BufferStrategy bs = this.getBufferStrategy();
		if(bs == null) {
			this.createBufferStrategy(3);
			return;
		}
		
		Graphics g = bs.getDrawGraphics();
		
		g.setColor(Color.red);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		g.dispose();
		bs.show();
	}
	
	
	public static void main(String args[]){
		new Game();
	}
	

}
