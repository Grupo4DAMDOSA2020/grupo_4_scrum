package com.iframe;

import java.awt.Graphics;

//Primer objeto para pruebas, extiende todos los futuros objetos con si mismo
public abstract class GameObject {
	
	protected int x, y;
	//Sistema de enumeracion de objetos
	protected ID id;
	//Datos de velocidada de desplazamiento
	protected int velX, velY;
	
	//Construccion basica para los objetos de GameObject
	public GameObject(int x, int y, ID id) {
		this.x = x;
		this.y = y;
		this.id = id;
		
	}
	
	public abstract void tick();	
	public abstract void render(Graphics g);
	
	//Metricas basicas para la gestion de objetos y la posicion de estos en la pantalla
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setId(ID id) {
		this.id = id;
	}
	
	public ID getId() {
		return id;
	}
}
