package com.iframe;

import java.awt.Graphics;

//Para inicializarlo existe la clase GameObject
public class Player  extends GameObject{
	
	//Se apoya en los datos disponibles en GameObject para determinar el estado de
	//cualquier objeto
	
	public Player(int x, int y, ID id) {
		super(x, y, id);
	}
	
	@Override
	public void tick() {
		
	}
	
	@Override
	public void render(Graphics g) {
		
	}
}
