
import javax.swing.*;
import java.awt.*;

public class prueba {
	
	private JFrame f;
	private JPanel p;
	private JButton b1;
	private JLabel lab;
	
		public prueba() {
			
			gui();
		}
	
		public void gui() { //Funcion interfaz de usuario
			
			f = new JFrame("prueba");
			f.setVisible(true);	
			f.setSize(600,400); // Tamaño de la ventana
			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Sin este comando aunque le demos a la cruz para salir no se nos cerraria el juego.
			
			p = new JPanel();
			
			b1 = new Jbutton("Jugar"); //Botones dentro de las ventanas
			lab = new JLabel("Prueba de juego"); // Titulo de la ventana
			
			p.add(b1);
			p.add(lab);
			
			f.add(p);
		}
	
		public static void main(String[] args]) {
			
			new prueba();
			
		}
	
}