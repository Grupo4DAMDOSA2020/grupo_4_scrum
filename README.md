# Grupo 4 SCRUM

Este repositorio está creado para el uso del proyecto de evaluación de la asignatura de Entornos de Desarrollo.

# 1er Sprint
* Product Owner: Mario Fernandez.
* Scrum Master: Yago Villar.


# 2º Sprint
* Product Owner: Pablo Calderón.
* Scrum Master: Alejandro Caldera.


# 3er Sprint
* Product Owner: Mario Fernández.
* Scrum Master: Alejandro Caldera.

1ª reunión: 15/05/2020 12:00 Asignarnos tareas

2ª reunión: 18/05/2020 17:00 Ponernos al día de lo trabajado y asignar tareas

3ª reunión: 22/05/2020 17:00 Hemos hablado con Sergio para que nos ayudase.

4ª reunión: 25/05/2020 18:00 Ponernos al día de lo trabajado y asignar tareas


